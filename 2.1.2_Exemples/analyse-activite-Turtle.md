# Objectifs
* Création d'un environnement de développement
* Utilisation d'un interpréteur
* Concepts
  * séquence
  * affectation
  * répétition (boucles)
  * fonction

# pré-requis à cette activité
Pas de pré-requis

# Durée de l'activité
4 heures

# Exercices cibles
* 1h30 : installation de l'environnement de développement et prise en main avec le 1er exercice (concept de séquence d'instructions), puis eExercice 2 à 5 (concept de séquence d'instructions, suite)

* 1h : Exercices de niveau 2 (8 à 11, 5 et 6 optionnels) (concept de boucle et d'affectation)

* 1h : Exercices de niveau 3 (12 à 15, 16 à 20 optionnels) (concept de fonction)

* 30m : Questions

# Description du déroulement de l'activité
Pendant 1h30 :
* Explication rapide du langage python
* Explication et installation de l'environnement de développement
* Création du premier programme dans l'interpréteur 
* Explication du concept de séquence
* Création du même programme dans visual studio code
* Exercices individuels : 2 à 5

Pendant 1h : 
* Explication du concept d'affectation et du concept de boucle
* Exercices individuels : 8 à 11
* Exercices pour les plus rapides : 5 et 6

Pendant 1h :
* Explication du concept de fonction
* Création de la fonction carré en commun
* Exercices individuels : 12 à 15
* Exercices pour les plus rapides : 16 à 20

Pendant 30 minutes : 
* Résumé 
* Compréhension des élèves et questions

# Anticipation des difficultés des élèves
Travail individuel avec aide de l'enseignant pour :
* l'utilisation de l'environnement de développement
* le langage python
* le déboggage
* les boucles, les fonctions

# Gestion de l'hétérogénéïté
* Repérage des élèves en difficulté avec soit:
  * l'environnement de développement
  * le langage python
  * les exercices (calcul des angles en math, par ex)
  * les élèves plus rapides peuvent faire tous les exercices